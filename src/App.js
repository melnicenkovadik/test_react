import React, {Component} from 'react';
import {connect} from "react-redux"
import {Route} from "react-router-dom"
import PropTypes from 'prop-types';

import Modal from "./_components/Modal";
import ImageList from "./_components/ImageList";

import {ImagesActionCreators} from "./_redux/imagesActionCreators"


class App extends Component {
    componentDidMount() {
        this.props.dispatch(ImagesActionCreators.getAllImages())
    }

    render() {
        return (
            <div>
                <ImageList images={this.props.img}/>
                <Route exact={true} path="/image/:imageId" component={Modal}/>
            </div>
        );
    }
}

App.propTypes = {
    Images: {
        img: PropTypes.array
    }
}

const mapStateToProps = (state) => {
    const {img} = state;
    return {
        img
    }
}


export default connect(mapStateToProps)(App);
