import {
    GET_BIG_IMAGE,
    GET_IMAGES,
    POST_COMMENT,
    REQUEST,
} from "./imagesConstants";

export const ImagesActionCreators = {
    getAllImages,
    getBigImage,
    postComment
}

export function getAllImages() {
    return dispatch => {
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        };

        fetch(`https://boiling-refuge-66454.herokuapp.com/images`, requestOptions)
            .then(data => data.json())
            .then(json => dispatch(success(json)));
    };

    function success(data) {
        return {type: GET_IMAGES, payload: {data}}
    }
}

function getBigImage(id) {
    return dispatch => {
        dispatch(request())
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        };

        fetch(`https://boiling-refuge-66454.herokuapp.com/images/${id}`, requestOptions)
            .then(data => data.json())
            .then(json => dispatch(success(json)));
    };

    function success(data) {
        return {type: GET_BIG_IMAGE, payload: {data}}
    }

    function request() {
        return {type: REQUEST}
    }
}

function postComment(id, name, comment) {
    return dispatch => {
        const requestOptions = {
            method: 'POST',
            body: JSON.stringify({
                name,
                comment
            }),
            headers: {'Content-Type': 'application/json'}
        };

        fetch(`https://boiling-refuge-66454.herokuapp.com/images/${id}/comments`, requestOptions)
            .then(data => dispatch(success(data)))
    };

    function success(data) {
        return {type: POST_COMMENT, payload: {name, comment}}
    }

}



