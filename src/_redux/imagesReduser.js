import {GET_IMAGES, GET_BIG_IMAGE, REQUEST, POST_COMMENT} from "./imagesConstants";

const initialState = {
    img: [],
    currentImage: {
        image: "",
        comments: [],
        loading: false
    }
}

export function Images(state = initialState, action) {
    switch (action.type) {
        case GET_IMAGES:
            return {
                ...state,
                img: action.payload.data
            }
        case GET_BIG_IMAGE:
            return {
                ...state,
                currentImage: {
                    ...action.payload.data,
                    loading: false
                }
            }
        case REQUEST:
            return {
                ...state,
                currentImage: {
                    ...state.currentImage,
                    loading: true
                }
            }
        case POST_COMMENT:
            return {
                ...state,
                currentImage: {
                    ...state.currentImage,
                    comments: [...state.currentImage.comments, {text: action.payload.comment}],
                    loading: false
                }
            }
        default:
            return state;
    }
}
