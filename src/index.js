import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from "redux";
import reduxThunk from 'redux-thunk';
import {Provider} from "react-redux";

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import App from './App';
import {Images} from "./_redux/imagesReduser";
import loger from 'redux-logger'

const store = createStore(Images, compose(
  applyMiddleware(
    reduxThunk, loger
  ),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/" component={App}/>
      </Switch>
    </Router>
  </Provider>

  ,
  document.getElementById('root')
);
