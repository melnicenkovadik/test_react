import React from 'react';
import {Link} from "react-router-dom";
import {Card} from "react-bootstrap";

const Image = ({id, url}) => {
    return (
      <Link to={`/image/${id}`}>
          <Card.Img className="image" key={id} alt={id} variant="top" src={url}/>
      </Link>
    );

}

export default Image;
