import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {Col, Container, Row, ListGroup, Image} from "react-bootstrap";
import {Link} from "react-router-dom";
import styled from 'styled-components'
import {Formik} from "formik";
import * as Yup from "yup";

import {ImagesActionCreators} from "../_redux/imagesActionCreators";


const Modal = (props) => {
  const [userName, setUserName] = useState('')
  const [userComment, setUserComment] = useState('')
  useEffect(() => {
    props.dispatch(ImagesActionCreators.getBigImage(parseInt(props.match.params.imageId, 10)))

  }, [])

  const submitHandler = (event) => {
    event.preventDefault()
    if (userName.length > 0 && userComment.length > 0) {
      props.dispatch(ImagesActionCreators.postComment(props.match.params.imageId, userName, userComment));

    }
  }

  const changeNameHandler = event => {
    setUserName(event.target.value)
  }

  const changeCommentHandler = event => {
    setUserComment(event.target.value)
  }


  const {currentImage, closeHandler} = props;
  const comments = currentImage && currentImage.comments.map((item, index) =>
    <ListGroup.Item key={index}>{item.text}</ListGroup.Item>
  )

  return (
    <ModalWrapper>
      <ModalBody className="modal-body">

        {props.currentImage.loading &&
        <p>ЗАГРУЗКА...</p>
        }

        {!props.currentImage.loading &&
        <Container className="bg-white p-2 w-100" bg='white' fluid="md">
          <Col className="close" xs={1} md={1}>
            <Link to="/">
              <button onClick={closeHandler}>X</button>
            </Link>
          </Col>
          <Row className=" m-2 bg-white">


            <Row>

              <Col className="float-right" xs={12} md={6}>
                {currentImage &&
                <Image fluid src={currentImage.url} alt="modal-image" rounded/>}
              </Col>
              <Col xs={12} md={6}>
                <br/>
                <br/>
                <Row xs={12} md={12} className='m-3'>
                  <Col xs={12} md={6}>
                    <p><b>Комментарии</b>:</p>

                    <ListGroup className="bg-white m-3" variant="flush">
                      {comments}
                    </ListGroup>
                  </Col>


                  <Col xs={12} md={6}>
                    <Formik
                      initialValues={{name: userName, comment: userComment}}
                      onSubmit={async (values) => {
                        await new Promise((resolve) => setTimeout(resolve, 500));
                        setUserComment('')
                        setUserName('')
                      }}
                      validationSchema={Yup.object().shape({
                        name: Yup.string()
                      })}
                    >
                      {(props) => {
                        const {
                          isSubmitting,
                        } = props;
                        return (
                          <form onSubmit={submitHandler}>
                            <input
                              id="name"
                              placeholder="Enter your name"
                              type="text"
                              onChange={changeNameHandler}
                            />

                            <input
                              id="comment"
                              placeholder="Enter your comment"
                              type="text"
                              onChange={changeCommentHandler}
                            />
                            <button type="submit" disabled={isSubmitting}>
                              Submit
                            </button>

                          </form>
                        );
                      }}
                    </Formik>

                  </Col>
                  </Row>
                </Col>
              </Row>
            </Row>
          </Container>
          }
        </ModalBody>
      </ModalWrapper>
    );
}

const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(0,0,0,.3);
`
const ModalBody = styled.div`
    height: max-content;
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`

const mapStateToProps = (state) => {
  const {currentImage} = state;
  return {
    currentImage
  }
}

export default connect(mapStateToProps)(Modal);
