import React from 'react';
import Image from "./Image";
import {Card, CardColumns, Container} from "react-bootstrap";
import {Header} from "./Header";

const ImageList = ({images}) => {
  const list = images.map(item => <Card key={item.id} className="text-center m-1">
      <Image id={item.id} url={item.url}/>
    </Card>
  )
  return (
    <Container>
      <Header/>
      <CardColumns bg="primary">
        {list}
      </CardColumns>
    </Container>
  );
}

export default ImageList
